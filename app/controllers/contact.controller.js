const Contact = require("../models/contact.model.js");

// Create and Save a new Contact
exports.create = (req, res) => {
  // Validasi
  if (!req.body) {
    res.status(400).send({
      message: "Tidak boleh kosong!"
    });
  }

  // Buat Contact
  const contact = new Contact({
    email: req.body.email,
    name: req.body.name,
    nohp: req.body.nohp
  });

  // Menyimpan Contact ke database
  Contact.create(contact, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Terjadi kesalahan dalam menyimpan data."
      });
    else res.send(data);
  });
};

// Mengambil semua data
exports.findAll = (req, res) => {
  Contact.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Terjadi kesalahan dalam mengambil data."
      });
    else res.send(data);
  });
};

// Mengambil satu detail kontak menggunakan id
exports.findOne = (req, res) => {
  Contact.findById(req.params.contactId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Tidak menemukan kontak dengan id : ${req.params.contactId}.`
        });
      } else {
        res.status(500).send({
          message: "Terjadi kesalahan mengambil kontak dengan id : " + req.params.contactId
        });
      }
    } else res.send(data);
  });
};

// Update Contact
exports.update = (req, res) => {
  // Validasi
  if (!req.body) {
    res.status(400).send({
      message: "Tidak boleh kosong!"
    });
  }

  console.log(req.body);

  Contact.updateById(
    req.params.contactId,
    new Contact(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Tidak menemukan kontak dengan id : ${req.params.contactId}.`
          });
        } else {
          res.status(500).send({
            message: "Terjadi kesalahan dalam update kontak dengan id : " + req.params.contactId
          });
        }
      } else res.send(data);
    }
  );
};

// Menghapus kontak dengan parameter contactId
exports.delete = (req, res) => {
  Contact.remove(req.params.contactId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Tidak menemukan kontak dengan id : ${req.params.contactId}.`
        });
      } else {
        res.status(500).send({
          message: "Tidak dapat menghapus kontak dengan id : " + req.params.contactId
        });
      }
    } else res.send({ message: `Berhasil menghapus kontak!` });
  });
};

