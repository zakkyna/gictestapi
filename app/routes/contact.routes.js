module.exports = app => {
  const contacts = require("../controllers/contact.controller.js");

  // Membuat kontak baru
  app.post("/buat", contacts.create);

  // Daftar list kontak
  app.get("/daftar", contacts.findAll);

  // Get detail kontak
  app.get("/kontak/:contactId", contacts.findOne);

  // Ubah data kontak
  app.put("/ubah/:contactId", contacts.update);

  // Hapus data kontak
  app.delete("/hapus/:contactId", contacts.delete);

};
