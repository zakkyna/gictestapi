const sql = require("./db.js");

// constructor
const Contact = function(contact) {
  this.email = contact.email;
  this.name = contact.name;
  this.nohp = contact.nohp;
};

Contact.create = (newContact, result) => {
  sql.query("INSERT INTO contacts SET ?", newContact, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("kontak dibuat: ", { id: res.insertId, ...newContact });
    result(null, { id: res.insertId, ...newContact });
  });
};

Contact.findById = (contactId, result) => {
  sql.query(`SELECT * FROM contacts WHERE id = ${contactId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("menemukan kontak: ", res[0]);
      result(null, res[0]);
      return;
    }

    // kontak tidak ditemukan
    result({ kind: "not_found" }, null);
  });
};

Contact.getAll = result => {
  sql.query("SELECT * FROM contacts", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("contacts: ", res);
    result(null, res);
  });
};

Contact.updateById = (id, contact, result) => {
  sql.query(
    "UPDATE contacts SET email = ?, name = ?, nohp = ? WHERE id = ?",
    [contact.email, contact.name, contact.nohp, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // kontak tidak ditemukan
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("Kontak terupdate: ", { id: id, ...contact });
      result(null, { id: id, ...contact });
    }
  );
};

Contact.remove = (id, result) => {
  sql.query("DELETE FROM contacts WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // kontak tidak ditemukan
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("telah terhapus kontak dengan id: ", id);
    result(null, res);
  });
};


module.exports = Contact;
